'use strict';
process.env.DEBUG = 'actions-on-google:*';

/* DialogFlow エージェントを取得します。 */
const {dialogflow} = require('actions-on-google');
const app = dialogflow();

/* Firebase インスタンスを取得します。 */
const functions = require('firebase-functions');

/* Firestore インスタンスを取得します。 */
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

/* 食事内容の聞き取りIntentの処理を実装します。 */
app.intent('食事内容の聞き取り', conv => {
    /* ユーザの発言から抽出したパラメータを取得します。 */
    let food = conv.parameters['food'];
    let amount = conv.parameters['number'];

    /* 発言をデータベースに保存します。 */
    return db.collection('users').doc('user1').collection('foods').doc().set({
        'food': food,
        'amount': amount
    }).then(() => {
        /* 保存が成功したら、次の食事内容を促します。 */
        conv.ask(food + ' ' + amount + 'グラムと記録しました。'
                + 'ほかには何を食べましたか？');
        return 0; /* 'then' required return a value. */
    }).catch(err => {
        /* 保存に失敗したら、エラー発生を伝えてアプリを終了します。 */
        console.log(err);
        conv.close(err);
    });
});

/* Fulfillment のデプロイ先を指定します。 */
exports.mealrecorder = functions.https.onRequest(app);
